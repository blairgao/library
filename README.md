**The Library Project**
This project uses basic Java that I learn from the course to represent a traditional library.

I create a class to represent the library itself (Library) and then a abstract class for items (Item) with subclasses of Books, CDs, DVDs and Periodicals.

The Library class will allow users to have items added to it and items removed from it. It will also be possible to borrow items from the library. It also allows users to look up books from an author. 

Periodicals cannot be borrowed. 

I create a series of JUnit tests to test the find, add, remove, and borrow functions .

