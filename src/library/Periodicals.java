package library;

public class Periodicals extends Item {
    public Periodicals(String itemName, String author, int ISBN) {
        super(itemName, author, ISBN);
    }

    @Override
    public boolean canBorrow() {
        return false;
    }
}
