package library;

public abstract class Item {
    /** The name of the item */
    public String itemName;
    /** The author of the item */
    public String author;
    /** The ISBN of the item */
    private int ISBN;

    public Item(String itemName, String author, int ISBN) {
        this.itemName = itemName;
        this.author = author;
        this.ISBN = ISBN;
    }

    public abstract boolean canBorrow();
}
