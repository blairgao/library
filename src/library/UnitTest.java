package library;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;


/** The suite of all JUnit tests for the library package.
 *  @author Blair
 */
public class UnitTest {

    String name = "James K. Moffitt Library";
    String address = "University of California\n" + "Berkeley, CA 94720-6000";
    String[] hours = new String[]{"Mon", "Tue", "Wed"};

    String bookName = "Harry Potter and the Deathly Hallows";
    String author = "J. K. Rowling";
    int ISBN = 0545010225;


    private Library moffitt;
    private Books hp;
    @Before
    public void setUp() {
        moffitt = new Library(name, address, hours);
        hp = new Books(bookName,author,ISBN);

    }

    @Test
    public void TestShowInfoLib() {
        moffitt.showInfoLibrary();
        assertEquals("James K. Moffitt Library", moffitt.libraryName);
        assertEquals("University of California\n" + "Berkeley, CA 94720-6000", moffitt.address);
        assertEquals(0, moffitt.getCollections().size());

    }

    @Test
    public void TestAdd() {
        moffitt.addToLibrary(hp);
        moffitt.showInfoLibrary();
        assertEquals(1, moffitt.getCollections().size());
        assertEquals(true, moffitt.getCollections().contains(hp));
    }

    @Test
    public void TestRemove() {
        moffitt.addToLibrary(hp);
        moffitt.removeFromLibrary(hp);
        moffitt.showInfoLibrary();
        assertEquals(0, moffitt.getCollections().size());
        assertEquals(false, moffitt.getCollections().contains(hp));

    }
    @Test
    public void TestBorrow() {
        moffitt.addToLibrary(hp);
        moffitt.borrowFromLibrary(hp);
        moffitt.showInfoLibrary();
        assertEquals(0, moffitt.getCollections().size());
        assertEquals(false, moffitt.getCollections().contains(hp));
    }

    @Test
    public void TestFind() {
        moffitt.addToLibrary(hp);
        moffitt.findBooksByAuthor("J. K. Rowling");

    }


}


