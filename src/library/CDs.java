package library;

public class CDs extends Item {
    public CDs(String itemName, String author, int ISBN) {
        super(itemName, author, ISBN);
    }

    @Override
    public boolean canBorrow() {
        return true;
    }
}
