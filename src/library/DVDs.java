package library;

public class DVDs extends Item {
    public DVDs(String itemName, String author, int ISBN) {
        super(itemName, author, ISBN);
    }

    @Override
    public boolean canBorrow() {
        return true;
    }
}
