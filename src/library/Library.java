package library;

import java.util.ArrayList;


/** Class that represents a complete library.
 *
 *  @author Blair Gao
 */
public class Library {

    /** The name of the library */
    public String libraryName;
    /** The address of the library */
    public String address;
    /** The working hours of the library */
    public String[] hours;
    /** The total collections in the library */
    private ArrayList<Item> collections;

    /** A new library with name, address, working hours, and total amount of collections. */
    public Library(String name, String address, String[] hours) {

        ArrayList<Item> collections = new ArrayList<Item>();

        this.libraryName = name;
        this.address = address;
        this.hours = hours;
        this.collections = new ArrayList<Item>();

    }

    /** Return all the collections in the library. */
    public ArrayList<Item> getCollections() {
        return collections;
    }


    /** Set the collections in the library. */
    public void setCollections(int Collections) {
        this.collections = collections;
    }


    /**The Library class will allow users to have items added to it and items removed from it.
     *  It will also be possible to borrow items from the library.
     */

    /** Allow users to have items added to Library*/
    public void addToLibrary(Item item){
        collections.add(item);
        System.out.println("Added " + item.itemName + " to the Library! ");
    };

    /** Allow users to have items removed from Library*/
    public void removeFromLibrary(Item item){
        if (!collections.contains(item)){
            System.out.println("Error: cannot remove if not exist in the library. ");
        } else {
            collections.remove(item);
            System.out.println("Removed " + item.itemName + " from the Library! ");
        }
    };

    /** Allow users to borrow items from Library*/
    public void borrowFromLibrary(Item item) {
        if (item.canBorrow() == false) {
            System.out.println("This item cannot be borrowed! Please go look for something else. ");
        } else if (!collections.contains(item)){
            System.out.println("Error: cannot borrow if not exist in the library. ");
        }
        else {
            collections.remove(item);
            System.out.println("You borrowed " + item.itemName + " from the Library! ");
        }
    }

    public void showInfoLibrary(){
        System.out.println("We have " + collections.size() + " items at the " + libraryName);
        for (Item item: collections) {
            System.out.println("They are " + item.itemName + " written by " + item.author + ".");
        }
    }

    public void findBooksByAuthor(String author){
        System.out.println("Books from " + author + ": ");
        for (Item item: collections) {
            if (item.author.equals(author)) {
                System.out.println(item.itemName);
            }
        }
    }



}